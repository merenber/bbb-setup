## Creating the database
On coffee, login as the `postgres` user, run `psql`, then run the following:
```sql
CREATE USER greenlight WITH PASSWORD 'replace_this_password';
CREATE DATABASE greenlight;
ALTER DATABASE greenlight OWNER TO greenlight;
```

## Running the playbook
Just the usual:
```
ansible-playbook playbook.yml
```
Once the playbook has finished, open `/root/bbb/env` and set the
value of `DB_PASSWORD`.

Also, place copies of `csclub-wildcard-chain.crt` and `csclub-wildcard.key`
(the wildcard CSC SSL certificate and key, respectively) in the directory
`/etc/nginx/ssl`. The key file must have permissions 0600.

Then, restart BBB:
```sh
bbb-conf --restart
```

## Installing Greenlight
See [here](https://docs.bigbluebutton.org/greenlight/gl-install.html) for
instructions.

**Important**: Add 'PORT=5000' to the top of your env file for Greenlight.
Otherwise it will listen on port 80, which NGINX is already listening on.

## Creating an administrator account
Theoretically, there is a [bundle command](https://docs.bigbluebutton.org/greenlight/gl-admin.html#creating-an-administrator-account)
which should be able to create an administrator account. However, when
I did this, I found that I was unable to login using the new admin
credentials. Might be because of LDAP. Here's my workaround:

1. Login once using your CSC credentials.
2. Log out.
3. On coffee, login as `postgres`, run `psql`, and run the following:
```sql
\c greenlight
UPDATE users SET role_id = 2 WHERE username = 'my_csc_username';
```

When you log back in, you should now be an admin.

## Editing the env file
Whenever you edit /root/bbb/env, make sure to restart the greenlight container
on xylitol with e.g. `systemct restart container-greenlight` and then run
`podman exec greenlight bin/rake db:migrate`.

## Greenlight customization
To ensure that future sysadmins automatically become Greenlight admins,
create a new role called "sysadmin" from the org settings in Greenlight.

To set a custom logo in the top left corner, go to 'Site Settings',
and replace the branding image URL. I'm using a small CSC logo hosted
on our git server in the csc-propaganda repo.
